%% Function that applies the method of Heun (2nd order) method and returns the solution y of an ODE 
%
%Inputs:
%            y_0 = initial condition;
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
% 
function y_heun = met_heun(y_0, dt, t_end, f)

t = 0:dt:t_end;
y_first = zeros(1,length(t));
y_heun = zeros(1,length(t));
y_heun(1) = y_0;

for k = 2:1:length(t)
    
    y_first(k) = y_heun(k-1) + dt * f(t, y_heun(k-1));
    y_heun(k) = y_heun(k-1) + (dt/2) * (f(t, y_heun(k-1)) + f(t, y_first(k)));

end


end