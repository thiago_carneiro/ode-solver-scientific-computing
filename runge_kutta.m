%% Function that applies the Runge-Kutta method (4th order) and returns the solution y of an ODE 
%
%Inputs:
%            y_0 = initial condition;
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
% 
function y_runge = runge_kutta(y_0, dt, t_end, f)

t = 0:dt:t_end;
y_first = zeros(1,length(t));
y_runge= zeros(1,length(t));
y_runge(1) = y_0;

for k = 2:1:length(t)
    
    y_1 = f(t, y_runge(k-1));
    y_2 = f(t, y_runge(k-1)+(dt/2)*y_1);
    y_3 = f(t, y_runge(k-1)+(dt/2)*y_2);
    y_4 = f(t, y_runge(k-1)+ dt*y_3);
    
    y_runge(k) = y_runge(k-1) + (dt/6) * (y_1 + 2*y_2 + 2*y_3 + y_4);

end


end