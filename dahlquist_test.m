%% Function that returns values of the Dahlquist test equation 
%
%Inputs:
%            x = imput value at a given time 't';
%            lamb = constant from the Dahlquist test equation;
% 
function x_diff = dahlquist_test(t, x)

lamb = -1;
x_diff = lamb * x;


end