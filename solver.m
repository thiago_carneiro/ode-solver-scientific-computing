function [y_euler, y_heun, y_runge] = solver(y_exact, y_0, dt, t_end, f)

err_euler = zeros(1,4);
err_euler_red = zeros(1,3);
err_heun = zeros(1,4);
err_heun_red = zeros(1,3);
err_runge = zeros(1,4);
err_runge_red = zeros(1,3);

for i = 1:1:4
    
    switch i
        case 1
            dt = 1;
        case 2
            dt = 1/2;
        case 3
            dt = 1/4;
        otherwise
            dt = 1/8;
    end
    
    % 1) explicit Euler method
    
    y_euler = expl_euler(y_0, dt, t_end, f); % calculate explicit Euler 
    
    % error
    sum1 = 0;
    for k = 1:1:length(y_euler)
        
        sum1 = sum1 + (y_euler(k) - y_exact(200*(k-1)*dt+1))^2;
        
    end
    
    err_euler(i) = sqrt(dt/t_end*sum1); % array of erros for each time step
    
    % 2) method of Heun
    
    y_heun = met_heun(y_0, dt, t_end, f); % calculate method of Heun
    
    % error
    sum2 = 0;
    for k = 1:1:length(y_heun)
        
        sum2 = sum2 + (y_heun(k) - y_exact(200*(k-1)*dt+1))^2;
        
    end
    
    err_heun(i) = sqrt((dt/t_end)*sum2); % array of erros for each time step
    
    % 3) Runge-Kutta method (fourth order)
     
    y_runge = runge_kutta(y_0, dt, t_end, f); % calculate Runge-Kutta method
    
    % error
    sum3 = 0;
    for k = 1:1:length(y_runge)
        
        sum3 = sum3 + (y_runge(k) - y_exact(200*(k-1)*dt+1))^2;
        
    end
    
    err_runge(i) = sqrt((dt/t_end)*sum3); % array of erros for each time step
    
    
    t = 0:dt:t_end;

    figure(2)
    hold on
    a1_2 = plot(0:0.005:5,y_exact,'blue');
    switch i 
        case 1
             a2 = plot(t,y_euler,'r');
        case 2
            a3 = plot(t,y_euler,'g');  
        case 3
            a4 = plot(t,y_euler,'k');     
        otherwise 
            a5 = plot(t,y_euler,'m'); 
    end
        
    title('Explicit Euler Method (1st order)')
    
    figure(3)
    hold on
    a1_3 = plot(0:0.005:5,y_exact,'blue');

    switch i 
        case 1
            a6 = plot(t,y_heun,'r');
        case 2
            a7 = plot(t,y_heun,'g');  
        case 3
            a8 = plot(t,y_heun,'k');     
        otherwise 
            a9 = plot(t,y_heun,'m'); 
    end

    figure(4)
    hold on
    a1_4 = plot(0:0.005:5,y_exact,'blue');

    switch i 
        case 1
            a10 = plot(t,y_runge,'r');
        case 2
            a11 = plot(t,y_runge,'g');  
        case 3
            a12 = plot(t,y_runge,'k');     
        otherwise 
            a13 = plot(t,y_runge,'m'); 
    end

end

figure(2)

title('Explicit Euler Method (1st order)')
xlabel('time')
ylabel('y(t)')

Me_1 = "Exact Solution";
M2 = "Explicit Euler Method (dt = 1)";
M3 = "Explicit Euler Method (dt = 1/2)";
M4 = "Explicit Euler Method (dt = 1/4)";
M5 = "Explicit Euler Method (dt = 1/8)";

legend([a1_2,a2,a3,a4,a5], [Me_1,M2,M3,M4,M5]);

figure(3)

title('Method of Heun (2nd order)')
xlabel('time')
ylabel('y(t)')

Me_2 = "Exact Solution";
M6 = "Method of Heun (dt = 1)";
M7 = "Method of Heun (dt = 1/2)";
M8 = "Method of Heun (dt = 1/4)";
M9 = "Method of Heun (dt = 1/8)";
legend([a1_3,a6,a7,a8,a9], [Me_2,M6,M7,M8,M9]);

figure(4)

title('Runge-Kutta method (4th order)')
xlabel('time')
ylabel('y(t)')

Me_3 = "Exact Solution";
M10 = "Runge-Kutta method (dt = 1)";
M11 = "Runge-Kutta method (dt = 1/2)";
M12 = "Runge-Kutta method (dt = 1/4)";
M13 = "Runge-Kutta method (dt = 1/8)";

legend([a1_4,a10,a11,a12,a13], [Me_3,M10,M11,M12,M13]);

for k = 1:1:3
    
    err_euler_red(k) = err_euler(k)/err_euler(k+1);
    err_heun_red(k) = err_heun(k)/err_heun(k+1);
    err_runge_red(k) = err_runge(k)/err_runge(k+1);
    
end

tables(err_euler, err_euler_red, err_heun, err_heun_red,... % output tables with error information
    err_runge, err_runge_red);   

end