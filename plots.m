function plots(dt, t_end, y_exact, y_euler, y_heun, y_runge)

t = 0:dt:t_end;

figure(2)
hold on
a1 = plot(0:0.005:5,y_exact,'blue');
a2 = plot(t,y_euler,'red');
title('Explicit Euler Method (1st order)')
xlabel('time')
ylabel('y(t)')

M1 = "Exact Solution";
M2 = "Explicit Euler Method (1st order)";
legend([a1,a2], [M1, M2]);


figure(3)
hold on
a1 = plot(0:0.005:5,y_exact,'blue');
a3 = plot(t,y_heun, 'k');
title('Method of Heun (2nd order)')
xlabel('time')
ylabel('y(t)')

M1 = "Exact Solution";
M3 = "Method of Heun (2nd order)";
legend([a1,a3], [M1, M3]);

figure(4)
hold on
a1 = plot(0:0.005:5,y_exact,'blue');
a4 = plot(t,y_runge,'black');
title('Runge-Kutta method (4th order)')
xlabel('time')
ylabel('y(t)')

M1 = "Exact Solution";
M4 = "Runge-Kutta method (4th order)";
legend([a1,a4], [M1, M4]);

end
