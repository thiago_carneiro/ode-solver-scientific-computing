%% Function that applies the explicit Euler method (1st order) and returns the solution y of an ODE 
%
%Inputs:
%            y_0 = initial condition;
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
% 
function y_euler = expl_euler(y_0, dt, t_end, f)

t = 0:dt:t_end;
y_euler = zeros(1,length(t)-1);
y_euler(1) = y_0;

for k = 2:1:length(t)
    
    y_euler(k) = y_euler(k-1) + dt * f(t, y_euler(k-1));

end

end
