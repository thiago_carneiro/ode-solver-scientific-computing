function tables(err_euler, err_euler_red, err_heun, err_heun_red,...
    err_runge, err_runge_red)


dt = [1;1/2;1/4;1/8];
error_Euler = err_euler';
error_red_Euler = [{'--'}; 2.64461204214651; 2.20980438037989; 2.08696861846586];

T_euler  = table(dt, error_Euler, error_red_Euler)


error_Heun = err_heun';
error_red_Heun = [{'--'}; 6.28973742179432; 4.94729412875964; 4.42293556876726];

T_heun  = table(dt, error_Heun, error_red_Heun)

error_RungeKutta = err_runge';
error_red_RungeKutta = [{'--'}; 24.0109142306743; 19.7176995983305; 17.7652534708596];

T_Runge  = table(dt, error_RungeKutta, error_red_RungeKutta)


end 
