function plot_van_de_pol(dt, t_end, y_heun_ext)

t = 0:dt:t_end;
figure(5)
hold on 
a5 = plot(t,y_heun_ext(1,:),'blue');
a6 = plot(t,y_heun_ext(2,:),'green');
a7 = plot(y_heun_ext(1,:), y_heun_ext(2,:),'red');
title('Van-der-Pol Oscillator solved by Method of Heun Extended');
xlabel('t and x');
ylabel('y');

M5 = "x vs. t";
M6 = "y vs. t";
M7 = "Trajectory in phase space y vs. x";

legend([a5,a6,a7], [M5, M6,M7]);

figure(6)

X = y_heun_ext(1,2:end);
Y = y_heun_ext(2,2:end);
u=y_heun_ext(1,2:end)-y_heun_ext(1,1:end-1);
v=y_heun_ext(2,2:end)-y_heun_ext(2,1:end-1);

meshgrid(X,Y);

u = u.*X;
v = v.*Y;
quiver(X,Y,u,v)
title('Van-der-Pol Oscillator Quiver Plot');
xlabel('x');
ylabel('y');

end
