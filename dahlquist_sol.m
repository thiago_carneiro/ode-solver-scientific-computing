%% function that returns the plot of the analytical solution of Dahlquist test equation
%
%            dt = time step size; 
%            t_end = end time;
%            lamb = constant from the Dahlquist test equation x(t) = e^(lamb*t)
% 
function y_exact = dahlquist_sol(dt, t_end)

lamb = -1;
t = 0:dt:t_end;
x = zeros(1,length(t));


for k = 1:1:length(t)
    
    y_exact(k) = exp(lamb*t(k));
    
end

figure(1)
hold on
plot(0:0.005:5,y_exact,'blue');
title('Exact Solution');
xlabel('t');
ylabel('x(t)');

end
