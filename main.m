clear all;
clc;

%% a) Use MATLAB to plot the function x(t) for t in [0; 5] in a graph.

y_0 = 1;
t_end = 5;
f = @dahlquist_test; % x' = lambda*x
dt = 0.005; % time step for the exact solution 

y_exact = dahlquist_sol(dt, t_end); % calculate the exact solution and plot the graph


%% b) Consider a general initial value problem
%           y' = f(y); y(0) = y0:
%           Implement the following explicit numerical methods:
%               1) explicit Euler method,
%               2) method of Heun,
%               3) Runge-Kutta method (fourth order)

[y_euler, y_heun, y_runge] = solver(y_exact, y_0, dt, t_end, f);

%% d) Extend the Heun solver from to also account for vector valued functions.
%       example with vector valued function 2x1:

dt = 0.1;
t_end = 20;
f = @von_der_paul_osc;
y_0 = [1;1];

y_heun_ext = met_heun_modified(y_0, dt, t_end, f);
plot_van_de_pol(dt, t_end, y_heun_ext);


