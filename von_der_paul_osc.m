function F_diff = von_der_paul_osc(t, u)

F_diff = zeros(2,1);

F_diff = [u(2);-u(1)+(1-u(1)^2)*u(2)];

end