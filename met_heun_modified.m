%% Function that applies the method of Heun (2nd order) to vectors and returns the vector solution y of an ODE 
%
%Inputs:
%            y_0 = initial condition;
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
% 
function y_heun_ext = met_heun_modified(y_0, dt, t_end, f)

[n,m] = size(y_0);

t = 0:dt:t_end;
y_first = zeros(n,length(t));
y_heun_ext = zeros(n,length(t));
y_heun_ext(1:n,1) = y_0;

for k = 2:1:length(t)
    
    y_first(1:n,k) = y_heun_ext(1:n,k-1) + dt * f(t, y_heun_ext(1:n,k-1));
    y_heun_ext(1:n,k) = y_heun_ext(1:n,k-1) + (dt/2) * (f(t, y_heun_ext(1:n,k-1)) + f(t, y_first(1:n,k)));

end



end

